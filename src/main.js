import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'
import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage)
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' 
Vue.use(Vuetify)
import FullCalendar from 'vue-full-calendar'
Vue.use(FullCalendar)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
// import Multiselect from 'vue-multiselect'
//   // register globally
// Vue.component('multiselect', Multiselect)

import '@fortawesome/fontawesome-free-webfonts/css/fontawesome.css'

// 使用するカテゴリーのファイルを import

import '@fortawesome/fontawesome-free-webfonts/css/fa-brands.css'
import '@fortawesome/fontawesome-free-webfonts/css/fa-regular.css'
import '@fortawesome/fontawesome-free-webfonts/css/fa-solid.css'
Vue.config.productionTip = false

const config = {
  apiKey: "AIzaSyCkUqCAK4JqrFb8YG7YY-HaDv1fizefyvk",
  authDomain: "vue-todo-c1f4f.firebaseapp.com",
  databaseURL: "https://vue-todo-c1f4f.firebaseio.com",
  projectId: "vue-todo-c1f4f",
  storageBucket: "vue-todo-c1f4f.appspot.com",
  messagingSenderId: "1056655501168"
};
firebase.initializeApp(config);

const init = () => {
  new Vue({
  router,
  render: h => h(App),
  }).$mount('#app')
  }
  // Wait for the deviceready event to start the render
  document.addEventListener("deviceready", () => {
  // eslint-disable-next-line
  console.log("Ready, Render the App");
  console.log(navigator.camera);
  init();
  });
  // If we are not in Cordova, manually trigger the deviceready event
  const isCordovaApp = (typeof window.cordova !== "undefined");
  if (!isCordovaApp) {
  document.dispatchEvent(new CustomEvent("deviceready", {}));
  }

