import Vue from 'vue'
import Router from 'vue-router'
import Signup from '@/components/Signup'
import AdminSignup from '@/components/AdminSignup'
import MenberSignup from '@/components/MenberSignup'
import m_signup from '@/components/m_signup'
import Signin from '@/components/Signin'
import Confirm from '@/components/Confirm'
import Syoutai from '@/components/syoutai'
import Setting from '@/components/Setting'
import Search from '@/components/Search'
import Renraku from '@/components/Renraku'
import Task from '@/components/Task'
import Home from '@/components/Home'
import DetailTask from '@/components/DetailTask'
import Detailhome from '@/components/Detailhome'
import Edit from '@/components/edit'
import NewTask from '@/components/NewTask'
import Gaihaku from '@/components/Gaihaku'
import Calendar from '@/components/Calendar'
import GaihakuCalendar from '@/components/GaihakuCalendar'
import MemberSearch from '@/components/MemberSearch'
import firebase from 'firebase'

Vue.use(Router)

let router =  new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: 'signin'
    },
    {
      path: '/',
      name: 'task',
      component: Task,
      meta: { requiresAuth: true }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      // meta: { requiresAuth: true }
    },
    {
      path: '/syoutai',
      name: 'Syoutai',
      component: Syoutai,
      meta: { requiresAuth: true }
    },
    {
      path: '/Setting',
      name: 'Setting',
      component: Setting,
      meta: { requiresAuth: true }
    },
    {
      path: '/renraku',
      name: 'renraku',
      component: Renraku,
      meta: { requiresAuth: true }
    },
    {
      path: '/newtask',
      name: 'newtask',
      component: NewTask,
      meta: { requiresAuth: true }
    },
    {
      path: '/gaihaku',
      name: 'gaihaku',
      component: Gaihaku,
      meta: { requiresAuth: true }
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
      meta: { requiresAuth: true }
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar,
      meta: { requiresAuth: true }
    },
    {
      path: '/gaihakucalendar',
      name: 'gaihakucalendar',
      component: GaihakuCalendar,
      meta: { requiresAuth: true }
    },
    {
      path: '/detailtask/:todo',
      name: 'detailtask',
      component: DetailTask,
      meta: { requiresAuth: true }
    },
    {
      path: '/Detailhome/:todo',
      name: 'Detailhome',
      component: Detailhome,
      meta: { requiresAuth: true }
    },
    {
      path: '/edit/:todo',
      name: 'edit',
      component: Edit,
      meta: { requiresAuth: true }
    },
    {
      path: '/confirm',
      name: 'confirm',
      component: Confirm,
      meta: { requiresAuth: true }
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/AdminSignup',
      name: 'AdminSignup',
      component: AdminSignup
    },
    {
      path: '/MenberSignup',
      name: 'MenberSignup',
      component: MenberSignup,
    },
    {
      path: '/m_signup',
      name: 'm_signup',
      component: m_signup,
    },
    {
      path: '/MemberSearch',
      name: 'MemberSearch',
      component: MemberSearch
    }
  ]
})
// ログインが完了していない場合にサインインページの飛ばす
router.beforeEach((to, from, next)=>{

  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  let currentUser = firebase.auth().currentUser　
  if (requiresAuth){
  if (!currentUser){
    next({
      path: '/signin',
      query: {redirect: to.fullPath}
    })
  }else{
    next()
  }
}else{
  next()
}
})

export default router