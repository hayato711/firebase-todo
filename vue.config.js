module.exports = {
    pluginOptions: {
      i18n: {
        locale: 'ja',
        fallbackLocale: 'ja',
        localeDir: 'locales',
        enableInSFC: false
      }
    },
  
    baseUrl: '',
    // outputDir: 'cordova_app/www',
    assetsDir: undefined,
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined
  }